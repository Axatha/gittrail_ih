﻿LoadFunctionLibrary "[ALM]Subject\TEST AUTOMATION\InfoHubAuto Library.qfl"

RepositoriesCollection.Add("[ALM] Subject\TEST AUTOMATION\Attribute Validations.tsr")
'Call Func_Screenshots()

datatable.ImportSheet "\\172.16.45.238\Test_Automation\SAP InfoHub\FileUploadData.xlsx", "TC31_TC39", "Global"

If Trim(Datatable("LoginType",1)) = "Multiple" Then
	Call Func_LoginToSourcingManager()
	Call captureScreenshot(WordFile)
Else
	Call Func_LoginToApplication()	
	Call captureScreenshot(WordFile)
End If	

Call Func_SelectSupplierpInWithEricssonpInInUnLockedState(Trim(Datatable("SupplierID", 1)))
Call captureScreenshot(WordFile)
'Call Func_ReadSupplierPIN("\\172.16.45.238\Test_Automation\SAP InfoHub\SAP TextFiles\ActivityLogEricPINs.txt")

Call Func_DownloadProductTemplate_FileUploadScenarios((Trim(Datatable("SupplierID",1))), (Trim(Datatable("FilePath",1))), Trim(Datatable("SupplierMaterialNo", 1)), Trim(DataTable.Value("EricssonMaterialNo",dtGlobal)))
Call captureScreenshot(WordFile)

Call Func_DeletingTabsIndownloadTemplate("Sourcing")
Call captureScreenshot(WordFile)

Call Func_FileUpload((Trim(Datatable("SupplierID",1))),(Trim(Datatable("FilePath",1))), Trim(Datatable("FileName", 1)))
	
	'Validating Results Summary
status = Func_CheckObjectexist("ResultSuccessful", "WebElement", "DIV")
If status = True Then
	Call Func_CheckObjectexist("Validated products1", "WebElement", "DIV")
	Call captureScreenshot(WordFile)
Else
	Reporter.ReportEvent micFail, "No error message should when uploaded material only with core tab by deleting the soruing tab or leaving the soruign tab blank", "Error message displays" 
	ExitAction
End If	
	
Call Func_Logout()
