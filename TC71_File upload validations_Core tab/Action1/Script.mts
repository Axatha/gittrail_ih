﻿LoadFunctionLibrary "[ALM]Subject\TEST AUTOMATION\InfoHubAuto Library.qfl"

RepositoriesCollection.Add("[ALM] Subject\TEST AUTOMATION\Attribute Validations.tsr")

datatable.ImportSheet "\\172.16.45.238\Test_Automation\SAP InfoHub\FileUploadData.xlsx", "TC31_TC39", "Global"

If Trim(Datatable("LoginType",1)) = "Multiple" Then
	Call Func_LoginToSourcingManager()
Else
	Call Func_LoginToApplication()	
End If	

Call Func_SelectSupplierpInWithEricssonpInInUnLockedState(Trim(Datatable("SupplierID", 1)))
'Call Func_ReadSupplierPIN("\\172.16.45.238\Test_Automation\SAP InfoHub\SAP TextFiles\ActivityLogEricPINs.txt")

Call Func_DownloadProductTemplate_FileUploadScenarios((Trim(Datatable("SupplierID",1))), (Trim(Datatable("FilePath",1))), Trim(Datatable("SupplierMaterialNo", 1)), Trim(DataTable.Value("EricssonMaterialNo",dtGlobal)))

Call Func_DeletingTabsIndownloadTemplate("Core")

Set ObjExcel = CreateObject("Excel.Application")
wait(5)
Set objWorkbook = objExcel.Workbooks.Open(Datatable("FilePath",1)&Trim(Datatable("FileName", 1))&".xlsx") 
objExcel.Visible = True
objWorkbook.Worksheets("Sourcing").Range("H6").Value = ""
objWorkbook.Worksheets("Sourcing").Range("I6").Value = ""
objWorkbook.Save
objWorkbook.Close
Set objWorkbook = Nothing
Set ObjExcel = Nothing

Call Func_FileUpload((Trim(Datatable("SupplierID",1))),(Trim(Datatable("FilePath",1))), Trim(Datatable("FileName", 1)))
	
	'Validating Results Summary
status = Func_CheckObjectexist("ResultSuccessful", "WebElement", "DIV")
If status <> True Then
	'Call Func_CheckObjectexist("Validated products1", "WebElement", "DIV")
	Call Func_DisplayErrorMessage()
Else
	Reporter.ReportEvent micFail, "error message should display when uploaded material only with sourcing tab by deleting the core tab or leaving the core tab blank", "Error message not displays" 
	ExitAction
End If	
	
Call Func_Logout()
